/** @type {import('next').NextConfig} */
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["i1.ytimg.com", "i2.ytimg.com", "i3.ytimg.com", "i4.ytimg.com"],
  },
}
