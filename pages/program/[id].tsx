import Head from "next/head"
import Link from "next/link"
import Image from "next/image"
import axios from "axios"
import {
  TableContainer,
  Table,
  TableRow,
  TableCell,
  TableBody,
  Paper,
  Container,
  Grid,
  Typography,
  CircularProgress,
  Box,
} from "@mui/material"
import CloudDownloadIcon from "@mui/icons-material/CloudDownload"
import { GetStaticProps, GetStaticPaths } from "next"
import AudioPlayer from "react-h5-audio-player"
import { useState } from "react"

import { Program } from "../../components/ProgramList"
import { TopBar } from "../../components/TopBar"
import { Layout } from "../../components/MenuLayout"

import config from "../../config.json"
const BASEURL = config.baseUrl

const drawerWidthMd = 250
const drawerWidthSm = 59

enum LoadingStatus {
  Loading,
  CanPlay,
  Error,
}

export default function ProgramPage({ postData }: { postData: Program }) {
  const [loadingStatus, setLoadingStatus] = useState(LoadingStatus.Loading)
  const url = "https://youtu.be/" + postData.yt_id
  const rows = [
    { name: "Title", data: postData.title },
    {
      name: "Author",
      data: postData.author ? (
        <Typography color="primary.dark">
          <Link href={"/author/" + postData.author}>{postData.author}</Link>
        </Typography>
      ) : (
        ""
      ),
    },
    {
      name: "Album",
      data: postData.album ? (
        <Typography color="primary.dark">
          <Link href={"/album/" + postData.album}>{postData.album}</Link>
        </Typography>
      ) : (
        ""
      ),
    },
    { name: "Publish Date", data: postData.publish_date },
    {
      name: "URL",
      data: (
        <a href={url} target="_blank" rel="noreferrer">
          <Typography color="primary.dark">{url}</Typography>
        </a>
      ),
    },
    {
      name: "Download",
      data:
        loadingStatus == LoadingStatus.Loading ? (
          <CircularProgress />
        ) : loadingStatus == LoadingStatus.CanPlay ? (
          <a
            href={`${BASEURL}/download/${postData.yt_id}`}
            download
            target="_blank"
            rel="noreferrer"
          >
            <CloudDownloadIcon sx={{ color: "primary.dark" }} />
          </a>
        ) : (
          "Not Available"
        ),
    },
  ]
  return (
    <>
      <Head>
        <title>{postData.title} - YT Program</title>
      </Head>
      <Layout>
        <TopBar pageTitle="Program" />

        <Container
          maxWidth="xl"
          sx={{ mt: 10, mb: loadingStatus == LoadingStatus.CanPlay ? 15 : 5 }}
        >
          <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 2, md: 3 }}>
            <Grid
              item
              xs={2}
              md={1}
              sx={{ position: "relative" }}
              minHeight={300}
            >
              <Image
                layout="fill"
                objectFit="contain"
                src={postData.thumbnail_url}
                alt={postData.thumbnail_url}
              ></Image>
            </Grid>
            <Grid item xs={2} md={2}>
              <TableContainer component={Paper}>
                <Table aria-label="simple table">
                  <TableBody>
                    {rows.map((row) => {
                      return (
                        <TableRow
                          key={row.name}
                          sx={{
                            "&:last-child td, &:last-child th": { border: 0 },
                          }}
                        >
                          <TableCell component="th" scope="row">
                            {row.name}
                          </TableCell>
                          <TableCell align="left">{row.data}</TableCell>
                        </TableRow>
                      )
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
          </Grid>
        </Container>
        <Box
          sx={{
            position: "fixed",
            bottom: 0,
            marginLeft: 0,
            display: loadingStatus == LoadingStatus.CanPlay ? "block" : "none",
            width: {
              xs: "100%",
              sm: `calc(100% - ${drawerWidthSm}px)`,
              md: `calc(100% - ${drawerWidthMd}px)`,
            },
            left: { xs: 0, sm: `${drawerWidthSm}px`, md: `${drawerWidthMd}px` },
          }}
        >
          <AudioPlayer
            src={`${BASEURL}/download/${postData.yt_id}`}
            onLoadedMetaData={() => setLoadingStatus(LoadingStatus.CanPlay)}
            onError={() => setLoadingStatus(LoadingStatus.Error)}
            preload="metadata"
          />
        </Box>
      </Layout>
    </>
  )
}

export const getStaticPaths: GetStaticPaths = async () => {
  const result = await axios.get(BASEURL + "/api/program")
  const data = result.data.data as string[]
  const paths = data.map((id) => ({ params: { id: id } }))

  return {
    paths,
    fallback: "blocking",
  }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
  let postData
  if (params) {
    const result = await axios.get(
      (BASEURL + "/api/program/" + params.id) as string
    )
    postData = result.data.data as Program
  }
  return {
    props: {
      postData,
    },
    revalidate: config.staticRevalidate, // In seconds
  }
}
