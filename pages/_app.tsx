import "../styles/globals.css"
import "../styles/audio-player.scss"

import type { AppProps } from "next/app"
import { ThemeProvider, createTheme, CssBaseline } from "@mui/material"

const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
})

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}
