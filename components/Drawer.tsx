import { FC } from "react"
import List from "@mui/material/List"
import ListItemIcon from "@mui/material/ListItemIcon"
import ListItemText from "@mui/material/ListItemText"
import IconButton from "@mui/material/IconButton"
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined"
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined"
import CollectionsBookmarkOutlinedIcon from "@mui/icons-material/CollectionsBookmarkOutlined"
import VideocamIcon from "@mui/icons-material/Videocam"

import {
  Typography,
  ListItemButton,
  ListItemButtonProps,
  ListItem,
} from "@mui/material"

import Link from "next/link"
import { useRouter } from "next/router"

export const TABS = {
  home: {
    value: "Home",
    rootUrl: "/",
  },
  author: {
    value: "Author",
    rootUrl: "/author",
    urls: ["/author", "/author/[id]"],
  },
  album: {
    value: "Album",
    rootUrl: "/album",
    urls: ["/album", "/album/[id]"],
  },
}

interface LinkListItemProps extends ListItemButtonProps {
  href?: string
}

const CustomItem: FC<LinkListItemProps> = (props) => {
  return (
    <Link href={props.href || "/"} passHref>
      <ListItemButton
        selected={props.selected}
        sx={{ height: 60, pl: { sm: 2, md: 5 } }}
      >
        <ListItemIcon>{props.children}</ListItemIcon>
        <ListItemText primary={props.title} />
      </ListItemButton>
    </Link>
  )
}

export const CustomDrawer = () => {
  const router = useRouter()
  const path = router.pathname
  return (
    <>
      <List sx={{ pt: 0 }} component="nav">
        <Link href={"/"} passHref>
          <ListItem sx={{ height: 60, pl: { sm: 1, md: 4 } }}>
            <ListItemIcon>
              <IconButton>
                <VideocamIcon sx={{ color: "primary.dark" }} />
              </IconButton>
            </ListItemIcon>
            <Typography color="primary.dark" noWrap>
              <Link href="/" passHref>
                YT Program
              </Link>
            </Typography>
          </ListItem>
        </Link>

        <CustomItem
          key={TABS.home.value}
          title={TABS.home.value}
          href={TABS.home.rootUrl}
          selected={path === TABS.home.rootUrl}
        >
          <HomeOutlinedIcon />
        </CustomItem>
        <CustomItem
          key={TABS.author.value}
          title={TABS.author.value}
          href={TABS.author.rootUrl}
          selected={TABS.author.urls.includes(path)}
        >
          <PersonOutlinedIcon />
        </CustomItem>
        <CustomItem
          key={TABS.album.value}
          title={TABS.album.value}
          href={TABS.album.rootUrl}
          selected={TABS.album.urls.includes(path)}
        >
          <CollectionsBookmarkOutlinedIcon />
        </CustomItem>
      </List>
    </>
  )
}
