import { FC, useEffect, useState } from "react"
import { useRouter } from "next/router"

import { Drawer, Box, Tabs, Tab } from "@mui/material"
import HomeOutlinedIcon from "@mui/icons-material/HomeOutlined"
import PersonOutlinedIcon from "@mui/icons-material/PersonOutlined"
import CollectionsBookmarkOutlinedIcon from "@mui/icons-material/CollectionsBookmarkOutlined"

import { CustomDrawer, TABS } from "./Drawer"

const drawerWidthMd = 250
const drawerWidthSm = 59

const tabStalye = {
  minHeight: "auto",
  padding: 0.5,
  fontSize: 10,
}
const tabIconStayle = {
  height: 20,
}

export const Layout: FC = ({ children }) => {
  const [hover, setHover] = useState(false)
  const [canHover, setCanHover] = useState(false) //prevent iPhone safari from triggering hover

  useEffect(() => {
    setCanHover(!window.matchMedia("(hover: none)").matches)
  })

  const router = useRouter()
  const path = router.pathname
  const tabValue =
    path == TABS.home.rootUrl
      ? TABS.home.value
      : TABS.author.urls.includes(path)
      ? TABS.author.value
      : TABS.album.urls.includes(path)
      ? TABS.album.value
      : "None"

  return (
    <>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: "none", sm: "block" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: {
              sm: hover && canHover ? drawerWidthMd : drawerWidthSm,
              md: drawerWidthMd,
            },
            overflow: "hidden",
            transition: "width 0.1s",
          },
        }}
        onMouseOver={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
        onClick={() => setHover(false)}
      >
        <CustomDrawer />
      </Drawer>
      <Box
        sx={{
          mt: 5,
          mb: { xs: 5, sm: 0 },
          flexGrow: 1,
          p: 3,
          width: {
            sm: `calc(100% - ${drawerWidthSm}px)`,
            md: `calc(100% - ${drawerWidthMd}px)`,
          },
          ml: { sm: `${drawerWidthSm}px`, md: `${drawerWidthMd}px` },
        }}
      >
        {children}
      </Box>
      {tabValue !== "None" && (
        <Tabs
          aria-label="icon label tabs example"
          value={tabValue}
          sx={{
            display: {
              xs: "block",
              sm: "none",
            },
            position: "fixed",
            bottom: 0,
            marginLeft: 0,
            width: "100%",
            backgroundColor: "background.paper",
            borderTop: 1,
            borderColor: "divider",
          }}
          variant="fullWidth"
        >
          <Tab
            icon={<HomeOutlinedIcon sx={tabIconStayle} />}
            value={TABS.home.value}
            label={TABS.home.value}
            onClick={() => router.push(TABS.home.rootUrl)}
            sx={tabStalye}
          />
          <Tab
            icon={<PersonOutlinedIcon sx={tabIconStayle} />}
            value={TABS.author.value}
            label={TABS.author.value}
            onClick={() => router.push(TABS.author.rootUrl)}
            sx={tabStalye}
          />
          <Tab
            icon={<CollectionsBookmarkOutlinedIcon sx={tabIconStayle} />}
            value={TABS.album.value}
            label={TABS.album.value}
            onClick={() => router.push(TABS.album.rootUrl)}
            sx={tabStalye}
          />
        </Tabs>
      )}
    </>
  )
}
