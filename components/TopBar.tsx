import {
  AppBar,
  Toolbar,
  Typography,
  InputBase,
  Box,
  IconButton,
} from "@mui/material"
import SearchIcon from "@mui/icons-material/Search"
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew"

import { styled, alpha } from "@mui/material/styles"
import React from "react"
import { useRouter } from "next/router"

const Search = styled("div")(({ theme }) => ({
  position: "relative",
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  "&:hover": {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  width: "100%",
  [theme.breakpoints.up("sm")]: {
    marginLeft: theme.spacing(1),
    width: "auto",
  },
}))

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}))

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
}))

const CustomAppBar: React.FC = ({ children }) => {
  const router = useRouter()
  const rootPages = ["/", "/author", "/album"].includes(router.pathname)

  return (
    <Box>
      <AppBar
        position="fixed"
        sx={{ pl: { xs: 0, sm: 49 + "px", md: 250 + "px" } }}
      >
        <Toolbar variant="dense">
          {!rootPages && (
            <IconButton onClick={() => router.back()} sx={{ mr: 2 }}>
              <ArrowBackIosNewIcon />
            </IconButton>
          )}
          {children}
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export const TopBarSearch: React.FC<{
  setKeywords: (value: string) => void
}> = ({ children, setKeywords }) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setKeywords(event.target.value as string)
  }

  return (
    <CustomAppBar>
      <Typography
        variant="h6"
        noWrap
        component="div"
        sx={{
          flexGrow: 1,
          display: { xs: "none", sm: "block" },
          color: "primary.dark",
        }}
      >
        {children}
      </Typography>
      <Search>
        <SearchIconWrapper>
          <SearchIcon />
        </SearchIconWrapper>
        <StyledInputBase
          placeholder="Search…"
          inputProps={{ "aria-label": "search" }}
          onChange={handleChange}
        />
      </Search>
    </CustomAppBar>
  )
}

export const TopBar: React.FC<{ pageTitle: String }> = ({ pageTitle }) => {
  const router = useRouter()
  return (
    <CustomAppBar>
      <Typography variant="h6" noWrap component="div" color="primary.dark">
        {pageTitle}
      </Typography>
    </CustomAppBar>
  )
}
