import React, { useState, useEffect, useRef, useCallback } from "react"
import axios from "axios"
import Link from "next/link"

import {
  Tooltip,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Grid,
  CircularProgress,
  Box,
} from "@mui/material"

import config from "../config.json"
const BASEURL = config.baseUrl

export interface Program {
  yt_id: string
  publish_date: string
  download_date?: string
  title: string
  album?: string
  author?: string
  status?: string
  thumbnail_url: string
}

export const ProgramCard: React.FC<{
  program: Program
}> = ({ program }) => {
  return (
    <Link href={"/program/" + program.yt_id} passHref>
      <Tooltip title={program.title} followCursor>
        <Card>
          <CardActionArea>
            <CardMedia
              component="img"
              height="150"
              src={program.thumbnail_url}
              alt={program.title}
            />

            <CardContent>
              <Box
                sx={{
                  height: 130,
                }}
              >
                <Typography
                  gutterBottom
                  variant="subtitle1"
                  component="div"
                  sx={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    display: "-webkit-box",
                    lineClamp: 3,
                    WebkitLineClamp: 3,
                    WebkitBoxOrient: "vertical",
                  }}
                >
                  {program.title}
                </Typography>
                <Typography variant="body1" color="text.secondary" noWrap>
                  {program.author}
                </Typography>
                <Typography variant="body1" color="text.secondary" noWrap>
                  {new Date(program.publish_date).toDateString()}
                </Typography>
              </Box>
            </CardContent>
          </CardActionArea>
        </Card>
      </Tooltip>
    </Link>
  )
}

const useProgramSearch = ({
  pageNumber,
  author,
  album,
  search,
}: {
  pageNumber: number
  author?: string
  album?: string
  search?: string
}) => {
  const [programs, setPrograms] = useState([] as Program[])
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)
  const [hasMore, setHasMore] = useState(true)
  const per_page = 24

  useEffect(() => {
    setLoading(true)
    setError(false)

    const params = new URLSearchParams({
      per_page: String(per_page),
      page: String(pageNumber),
      search: search || "",
      author: author || "",
      album: album || "",
    })

    let cancel: () => void

    axios
      .get(BASEURL + "/api/search/?" + params.toString(), {
        cancelToken: new axios.CancelToken((c) => (cancel = c)),
      })
      .then((response) => {
        const result = response.data.data as Program[]
        if (result.length == 0) {
          setHasMore(false)
          setLoading(false)
        }
        setPrograms((prev) => {
          return [...prev, ...result]
        })

        if (result.length != per_page) {
          setHasMore(false)
        }
        setLoading(false)
      })
      .catch((e) => {
        if (axios.isCancel(e)) return
        setError(true)
      })
    return () => cancel()
  }, [search, author, album, pageNumber])

  useEffect(() => {
    setPrograms([])
    setHasMore(true)
  }, [search, author, album])
  return { programs, loading, error, hasMore }
}

export const ProgramListCard: React.FC<{
  keywords?: string
  author?: string
  album?: string
}> = ({ keywords, author, album }) => {
  const [pageNumber, setPageNumber] = useState(1)
  const { programs, loading, error, hasMore } = useProgramSearch({
    pageNumber: pageNumber,
    search: keywords,
    author: author,
    album: album,
  })

  useEffect(() => {
    setPageNumber(1)
  }, [keywords, author, album])

  //handle infinite scroll
  const observer = useRef() as React.MutableRefObject<IntersectionObserver>
  const lastElementRef = useCallback(
    (node) => {
      if (loading || !hasMore) {
        return
      }

      if (observer.current) observer.current.disconnect()

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          setPageNumber(pageNumber + 1)
        }
      })
      if (node) observer.current.observe(node)
    },
    [loading, hasMore, pageNumber]
  )

  const LoadingIndicator: React.FC<{ loading: boolean }> = ({ loading }) => {
    if (loading) {
      return (
        <Box
          sx={{
            width: 40,
            height: 40,
            m: "auto",
            mt: 10,
            mb: 10,
          }}
        >
          <CircularProgress ref={lastElementRef} />
        </Box>
      )
    } else {
      return <div></div>
    }
  }

  return (
    <>
      <Grid
        container
        spacing={{ xs: 2, md: 3 }}
        columns={{ xs: 2, sm: 12, lg: 12 }}
      >
        {programs.map((program, index) => {
          let lastElem
          if (programs.length === index + 4) {
            lastElem = <div></div>
          } else lastElem = <div></div>

          return (
            <Grid item xs={2} sm={4} lg={3} key={index}>
              {lastElem}
              <ProgramCard program={program} />
            </Grid>
          )
        })}
      </Grid>
      <LoadingIndicator loading={hasMore} />
    </>
  )
}
